// const app = require('C:/Users/Rudolf/OneDrive - Universidad Técnica Federico Santa María/Escritorio/TEL-335/Certamen2PRueba/Certamen2/build/index.js') 
import request from 'supertest'
import connection from '../src/ormConnection'
import server from '../src/index'


/**
 * 1. Manda diversos usuarios via HTTP POST requests.
 * 2. Elimna uno de los usuarios ingresados e intenta elimnar un usuario inexistente con HTTP DELETE requests.
 */



describe('Prueba POST y DELETE del endpoint /user ', ()=>{
    let RamiroID:string
    beforeAll(async () => {
        await connection.create()
    })

    afterAll(async (done) => {
        await connection.close()
    })
    it('Prueba POST user Ramiro', async done => {
        const response:any = await request(server).post('/user')
        .send({
            "name": "Ramiro Cuadra",
            "age": 35,
            "email": "ramiro.cuadra@gmail.com",
            "favoriteGenres": "Accion, Ciencia ficcion, Comedia"
        })
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        
        expect(response.statusCode).toBe(200)
        expect(response.body).toBe('OK')
        console.error
        expect(response.body.message).toBe('User created correctly')
        RamiroID = response.body.userId
        
        done()
    })

    it('Prueba POST user Jose', async done => {
        const response:any = await request(server).post('/user')
        .send({
            "name": "Jose Pino",
            "age": 24,
            "email": "jose.pino@gmail.com",
            "favoriteGenres": "Anime, Terror, Comedia"
        })
        expect(response.status).toBe(200)
        expect(response.body.message.status).toBe('OK')
        expect(response.body.message.message).toBe('User created correctly')
        done()
    })

    it('Prueba POST user Nicolas', async done => {
        const response:any = await request(server).post('/user')
        .send({
            "name": "Nicolas Gallardo",
            "age": 24,
            "email": "nicolas.gallardo@gmail.com",
            "favoriteGenres": "Accion, Terror, Anime"
        })
        expect(response.status).toBe(200)
        expect(response.body.message.status).toBe('OK')
        expect(response.body.message.message).toBe('User created correctly')
        done()
    })

    it('Prueba POST user Cristobal', async done => {
        const response:any = await request(server).post('/user')
        .send({
            "name": "Cristobal Hernandez",
            "age": 24,
            "email": "cristobal.hernandez@gmail.com",
            "favoriteGenres": "Accion, Ciencia Ficcion, Anime, Drama"
        })
        expect(response.status).toBe(200)
        expect(response.body.message.status).toBe('OK')
        expect(response.body.message.message).toBe('User created correctly')
        done()
    })

    it('Prueba DELETE user Ramiro', async done => {
        const response:any = await request(server).delete('/user/' + RamiroID)

        expect(response.status).toBe(200)
        expect(response.body.message.status).toBe('OK')
        expect(response.body.message.message).toBe('User deleted correctly')
        done()
    })

    it('Prueba DELETE ID Falsa', async done => {
        const response:any = await request(server).delete('/user/WrongID')

        expect(response.status).toBe(400)
        expect(response.body.message.status).toBe('NOK')
        expect(response.body.message.message).toBe('User not found')
        done()
    })
  
})