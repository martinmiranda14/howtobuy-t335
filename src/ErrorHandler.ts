export const errorMessage  = (error: string) => {
    return {
        status: "NOK",
        message: error
    }
}