import express, { Express } from 'express'
import bodyParser from 'body-parser'
import connection from './ormConnection'

// Controllers
import * as rootController from './controller/rootController'
import * as moviesController from './controller/moviesController'
import * as userController from './controller/userController'

var cors = require('cors')
const server: Express = express()

server.use(cors())
server.use(bodyParser.json())
server.use(bodyParser.raw())
server.use(bodyParser.text())
server.use(bodyParser.urlencoded({
    extended: true
}))

const connect = connection.create

if (!connect)
    console.log('Error en la conexión')

console.log('Conexión creada')
// Services
server.get('/', rootController.sendDeafultMessage)
server.get('/movies', moviesController.sendMoviesInfo)
server.get('/user/:userId', userController.getUser)
server.get('/users', userController.getUsers)
server.post('/movie', moviesController.insertMovie)
server.post('/movies', moviesController.insertMovies)
server.post('/user', userController.insertUser)
server.delete('/user/:userId', userController.deleteUser)
server.put('/user/:userId/favorites', userController.addFavoriteMovie)
server.delete('/user/:userId/favorites/:movieId', userController.deleteFavoriteMovie)
server.get('/user/:userId/favorites', userController.getFavoriteMovies)

server.listen(4000, () => {
    console.log('Server listening at port 4000')
})

export default server