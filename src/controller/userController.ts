import * as userRepository from '../repository/userRepository'
import { Request, Response } from 'express'
import { errorMessage } from '../ErrorHandler'


export const insertUser = async (request: Request, response: Response) => {
    const res = await userRepository.insertUserRepository(request.body).catch( () => errorMessage("There was an error creating user"))
    response.json(res)
}

export const deleteUser = async (request: Request, response: Response) => {
    const res = await userRepository.deleteUserRepository( parseInt(request.params.userId) ).catch( () => errorMessage("TThere was an error deleting user"))
    response.json(res)
}

export const getUser = async (request: Request, response: Response) => {
    const res = await userRepository.getUserRepository( request.params.userId ).catch( () => errorMessage("There was an error retrieving the User"))
    response.json(res)
}

export const getUsers = async (request: Request, response: Response) => {
    const res = await userRepository.getUsersRepository().catch( () => errorMessage("There was an error retrieving Users"))
    response.json(res)
}

export const addFavoriteMovie = async (request: Request, response: Response) => {
    const res = await userRepository.addFavoriteMovieRepository( request.params.userId , request.body ).catch( () => errorMessage("There was an error adding movie info"))
    response.json(res)
}

export const deleteFavoriteMovie = async (request: Request, response: Response) => {
    const res = await userRepository.deleteFavoriteMovieRepository( request.params.userId , request.params.movieId ).catch( () => errorMessage("There was an error removing movie info"))
    response.json(res)
}

export const getFavoriteMovies = async (request: Request, response: Response) => {
    const res = await userRepository.getFavoriteMoviesRepository( request.params.userId ).catch( () => errorMessage("There was an error retrieving favorites"))
    response.json(res)
}


