import { Request, Response } from 'express'
import * as moviesRepository from '../repository/moviesRepository'
import { errorMessage } from '../ErrorHandler'


export const sendMoviesInfo = async (request: Request, response: Response) => {
    const res = await moviesRepository.getMoviesData().catch( () => errorMessage("There was an error retrieving Movies"))
    response.json(res)
}

export const insertMovie = async (request: Request, response: Response) => {
    const res = await moviesRepository.newMovieRepository(request.body).catch( () => errorMessage("There was an error inesrting Movie"))
    response.json(res)
}

export const insertMovies = async (request: Request, response: Response) => {
    const res = await moviesRepository.newMoviesRepository(request.body).catch( () => errorMessage("There was an error interting Movies"))
    response.json(res)
}