import { Entity, Column, ObjectIdColumn, ObjectID } from 'typeorm'

@Entity()
export class Movie {

    @ObjectIdColumn()
    movieId: string

    @Column()
    title: string

    @Column()
    usGross: number

    @Column()
    worldWideGross: number

    @Column()
    usDVDSales: number | null

    @Column()
    productionBudget: number

    @Column()
    releaseDate: string

    @Column()
    mpaaRating: string

    @Column()
    runningTimeMin: number | null

    @Column()
    distributor: string

    @Column()
    source: string

    @Column()
    majorGenre: string
    
    @Column()
    creativeType: string

    @Column()
    director: string

    @Column()
    rottenTomatoesRating: number

    @Column()
    imdbRating: number

    @Column()
    imdbVotes: number
}