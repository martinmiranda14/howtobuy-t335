import { Entity , PrimaryColumn, Column, ObjectIdColumn} from 'typeorm'
import { Movie } from "./Movie";

@Entity()
export class Genre{

    @ObjectIdColumn()
    id: string

    @Column()
    tipo: string

    @Column(type => Movie)
    movies: Movie[];
}