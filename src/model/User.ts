import { Entity, Column, ObjectIdColumn, ObjectID} from 'typeorm'
import { Movie } from "./Movie"
import { Genre } from "./Genres"

@Entity()
export class User {

    @ObjectIdColumn()
    id: ObjectID

    @Column()
    nombre: string

    @Column()
    edad: number

    @Column()
    email: string
    
    @Column(type => Genre)
    generosFav: Genre[];

    @Column(type => Movie)
    peliculasFav: Movie[] 
    
}