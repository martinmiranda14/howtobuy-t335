import "reflect-metadata"
import { User } from '../model/User'
import { Movie } from '../model/Movie'
import { Genre } from '../model/Genres'
import { getMongoRepository } from 'typeorm'
import { ObjectID } from 'mongodb';
import { errorMessage } from '../ErrorHandler'


export const insertUserRepository = async (userData: any) => {
    const user = new User()
    
    const favoriteGenresArray: string[] = userData.favoriteGenres.split(", ") 
    
    var genres: Genre[] = [];
    
    for (let index :number = 0; index < favoriteGenresArray.length; index++) {
        const element: string = favoriteGenresArray[index]
        var genre: Genre= await getMongoRepository(Genre).findOne( {tipo: element} )
        if (!genre) {
            genre = new Genre()
            genre.tipo = element
            await getMongoRepository(Genre).save(genre).catch( (error:any) => { console.log("Error: ", error) })
        }
        genres.push(genre)
    } 
    console.log(genres)

    user.nombre = userData.name
    user.edad = userData.age
    user.email = userData.email
    user.generosFav = genres
    
    await getMongoRepository(User).save(user).catch( () => errorMessage("There was an error creating user") ) 

    return {
        status: 'OK',
        message: 'User created correctly',
        userId: user.id
    }
}


export const deleteUserRepository = async (userId: any) => {
    const user = await getMongoRepository(User).findOne( userId )
    console.log(!user);
    if (!user) 
        return errorMessage("User not found")

    await getMongoRepository(User).remove(user).catch( () => errorMessage("There was an error deleting user") )
    
    return {
        status: "OK",
        message: "User deleted correctly"
    }
}


export const getUsersRepository = async () => {
    const users: User[] = await getMongoRepository(User).find()
    if (!users.length) 
        return errorMessage("There are not users registered")

    console.log(users)
    return {
        status: "OK",
        users
    }
}


export const getUserRepository = async (userId: string) => {
    const user = await getMongoRepository(User).findOne( userId )
    if (!user) 
        return errorMessage("User not found")

    return {
        status: "OK",
        user
    }
}


export const addFavoriteMovieRepository = async (userId: string , movieAdd: any) => {
    const user = await getMongoRepository(User).findOne( userId )
    if (!user) 
        return errorMessage("User not found")
    
    let peliculasFav:any[] = []
    let movie: Movie = await getMongoRepository(Movie).findOne( {where: { title: { $eq:movieAdd.title } } })

    if ( typeof movie === 'undefined' || !movie ) 
        return errorMessage("Movie not in the repository")

    if(typeof user.peliculasFav !== 'undefined' && user.peliculasFav !== null)
        peliculasFav= user.peliculasFav

    peliculasFav.push(movie)
    
    await getMongoRepository(User).update(userId, {peliculasFav:peliculasFav }).catch( () => errorMessage("There was an error adding movie info") )
    
    return {
        status: "OK",
        message: "Movie added correctly",
        userId: user.id,
        movieId: movie.movieId
    }
}


export const deleteFavoriteMovieRepository = async (userId: string, movieId: string) => {
    const user = await getMongoRepository(User).findOne( userId )
    if (!user) 
        return errorMessage("User not found")

    let peliculasFav:any[] = []

    if(typeof user.peliculasFav !== 'undefined' && user.peliculasFav !== null)
        peliculasFav = user.peliculasFav
    else
        return errorMessage("There's no movie with that ID in your favorite list")

    for (let index = 0; index < peliculasFav.length; index++) {
        const movie = peliculasFav[index];
        if (movie.movieId == new ObjectID(movieId) )  
            peliculasFav.splice(index,1)
    }
    // var index = peliculasFav.findIndex((movie) => movie.movieId == movieId)
    // if(  != -1)
    //     peliculasFav.splice()
    // await getMongoRepository(User).remove()

    await getMongoRepository(User).update(userId, {peliculasFav:peliculasFav }).catch( () => errorMessage("There was an error adding movie info") )
    
    return {
        status: "OK",
        message: "Movie removed from user favorites list"
    }
}

    
export const getFavoriteMoviesRepository = async ( userId : string) => {
    const user = await getMongoRepository(User).findOne( userId )
    if (!user) 
        return errorMessage("There are no user with this ID registered")

    let peliculasFav: any[] = []

    if(typeof user.peliculasFav === 'undefined' || user.peliculasFav === null)
        return {
            status: "OK",
            message: "User has no favorites movies"
        }        
    
    peliculasFav = user.peliculasFav

    return {
        status: "OK",
        peliculasFav
    }
}

