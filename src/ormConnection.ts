import { createConnection, getConnection } from 'typeorm'

const connection: any= {
    async create(){
      await createConnection();
    },
  
    async close(){
      await getConnection().close(); 
    },
  
    async clear(){
      const connection: any = getConnection();
      const entities: any[] = connection.entityMetadatas;
  
      entities.forEach(async (entity:any) => {
        const repository: any= connection.getRepository(entity.name);
        await repository.query(`DELETE FROM ${entity.tableName}`);
      })
    },
  }

  export default connection