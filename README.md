# Proyecto TEL335 

Proyecto de sistema de alacena online , primer avance basado en el certamen 1 de Rudolf

Se uso TypeORM, MongoDB, Express, Jest, Typescript y Docker.

## Instrucciones de instalación

```
npm install
```

## Levantando el proyecto

```
docker-compose up -d

npm start

```
## Agregando Peliculas a la BD via HTTP POST:
```
POST localhost:4000/movies 
```
con un body con estructura por ejemplo:

```

[
    {title: "The Land Girls", usGross: 146083, worldWideGross: 146083, usDVDSales: null, productionBudget: 8000000, releaseDate: "Jun 12 1998", mpaaRating: "R", runningTimeMin: null, distributor: "Gramercy", source: null, majorGenre: null, creativeType: null, director: null, rottenTomatoesRating: null, imdbRating: 6.1, imdbVotes: 1071},
    {title: "First Love, Last Rites", usGross: 10876, worldWideGross: 10876, usDVDSales: null, productionBudget: 300000, releaseDate: "Aug 07 1998", mpaaRating: "R", runningTimeMin: null, distributor: "Strand", source: null, majorGenre: "Drama", creativeType: null, director: null, rottenTomatoesRating: null, imdbRating: 6.9, imdbVotes: 207},
    {title: "I Married a Strange Person", usGross: 203134, worldWideGross: 203134, usDVDSales: null, productionBudget: 250000, releaseDate: "Aug 28 1998", mpaaRating: null, runningTimeMin: null, distributor: "Lionsgate", source: null, majorGenre: "Comedy", creativeType: null, director: null, rottenTomatoesRating: null, imdbRating: 6.8, imdbVotes: 865},
    {title: "Let's Talk About Sex", usGross: 373615, worldWideGross: 373615, usDVDSales: null, productionBudget: 300000, releaseDate: "Sep 11 1998", mpaaRating: null, runningTimeMin: null, distributor: "Fine Line", source: null, majorGenre: "Comedy", creativeType: null, director: null, rottenTomatoesRating: 13, imdbRating: null, imdbVotes: null},
    {title: "Slam", usGross: 1009819, worldWideGross: 1087521, usDVDSales: null, productionBudget: 1000000, releaseDate: "Oct 09 1998", mpaaRating: "R", runningTimeMin: null, distributor: "Trimark", source: "Original Screenplay", majorGenre: "Drama", creativeType: "Contemporary Fiction", director: null, rottenTomatoesRating: 62, imdbRating: 3.4, imdbVotes: 165},
    {title: "Mississippi Mermaid", usGross: 24551, worldWideGross: 2624551, usDVDSales: null, productionBudget: 1600000, releaseDate: "Jan 15 1999", mpaaRating: null, runningTimeMin: null, distributor: "MGM", source: null, majorGenre: null, creativeType: null, director: null, rottenTomatoesRating: null, imdbRating: null, imdbVotes: null},
    {title: "Following", usGross: 44705, worldWideGross: 44705, usDVDSales: null, productionBudget: 6000, releaseDate: "Apr 04 1999", mpaaRating: "R", runningTimeMin: null, distributor: "Zeitgeist", source: null, majorGenre: null, creativeType: null, director: "Christopher Nolan", rottenTomatoesRating: null, imdbRating: 7.7, imdbVotes: 15133},
]

```

No se cual es el maximo de caracteres que se pueden enviar, pero no es infinito xD

## Listando las peliculas que hay en la BD via HTTP GET:
```
GET localhost:4000/movies
```


## Para agregar un usuario la estructura del body tiene que ser:
JSON
```
{
	"name": "Ramiro Cuadra",
        "age": 35,
        "email": "ramiro.cuadra@gmail.com",
        "favoriteGenres": "Accion, Ciencia ficcion, Comedia"
}
```


## Tambien agregue el poder ver un usuario en especifico o todos los usuarios
```
usuario en espeifico: GET localhost:4000/usuario/:ID
todos los usuarios: GET localhost:4000/usuarios
```


## Ejecutándo los tests

```
npm run test
```

